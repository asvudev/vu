from .base import *

INSTALLED_APPS += (
    'debug_toolbar',
)

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG = True

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'config.settings.dev.show_toolbar'
}

def show_toolbar(request):
    """
    Modified function to determine whether to show the toolbar on a given page.
    """

    if request.is_ajax():
        return False

    return bool(DEBUG)

STATIC_ROOT = STATIC_ROOT = os.getenv('STATIC_ROOT', os.path.join(PROJECT_DIR, 'collected_static'))

try:
    from .local_override import *
except ImportError:
    pass
