"""
Django settings for vu_site.

"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import dj_database_url
from django.core.exceptions import ImproperlyConfigured

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
PROJECT_DIR = os.path.dirname(BASE_DIR)

DEFAULT_SECRET_KEY = '__default_key'
SECRET_KEY = os.getenv('SECRET_KEY', DEFAULT_SECRET_KEY)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Custom Authentication / Authorization

AUTHENTICATION_BACKENDS = (
    'django_cas.backends.CASBackend',
    'ldapauth.backends.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# These settings are for the universal login system. Unless these change, there's no reason to modify them.
CAS_SERVER_URL = 'https://websso.wwu.edu/cas/'
CAS_VERSION = '1'

LDAP_SOURCES = {
    'wwu': {
        'server': 'ldap://ldap.ad.wwu.edu',
        'dn': 'cn={0},cn=builtin,dc=univ,dc=dir,dc=wwu,dc=edu'.format(os.getenv('WWU_LDAP_USER')),
        'BINDPASSWORD': os.getenv('WWU_LDAP_PASSWORD'),
        'base': 'DC=univ,DC=dir,DC=wwu,DC=edu'
    }
}

# users who are in this ldap group will be given superuser access
LADP_AUTH_SUPERUSER_GROUP = 'grp.union.admin.web'

# Application definition

INSTALLED_APPS = (

    # Standard set of apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Custom Authentication / Authorization
    'django_cas',
    'ldapauth',
    'apps.main',
    'apps.destinations'
)

MIDDLEWARE_CLASSES = (
    # Standard set of middleware classes
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',

    # Custom Authentication Middleware
    'django_cas.middleware.CASMiddleware',
    'ldapauth.middleware.StaffMiddleware',

)


ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# include the site-wide static assets
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static', 'assets'), os.path.join(PROJECT_DIR, 'compiled_static')]

WSGI_APPLICATION = 'config.wsgi.application'

MEDIA_ROOT = os.getenv('MEDIA_ROOT')
MEDIA_URL = os.getenv('MEDIA_URL')

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DB_URI = os.getenv('DB_URI')

if not DB_URI:
    raise ImproperlyConfigured('Missing database config. Expecting environment variable DB_URI. See https://github.com/kennethreitz/dj-database-url for details.')

DATABASES = {
    'default': dj_database_url.parse(DB_URI)
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.getenv('STATIC_ROOT')
