from .base import *
from django.core.exceptions import ImproperlyConfigured

if SECRET_KEY == DEFAULT_SECRET_KEY:
    raise ImproperlyConfigured('In production you must provide a SECRET_KEY that\'s different from the default.')