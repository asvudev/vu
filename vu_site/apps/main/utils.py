from datetime import date
from os.path import splitext
from django.utils.text import slugify


def upload_file_path(instance, filename):
    """Return the file path appropriate for the given model."""
    base_filename, file_extension = splitext(filename)
    return 'up/%s/%s%s' % (date.today().strftime('%Y/%m'), slugify(base_filename), file_extension)
