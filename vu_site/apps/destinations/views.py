from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import Service

def discover(request):
    return render(request, 'discover.html')

def plan(request):
    return render(request, 'plan.html')

# Create your views here.
class ServiceListView(ListView):
    model = Service
    template_name = "list.html"

class ServiceDetailView(DetailView):
    model = Service
    template_name = "detail.html"
    context_object_name = "service"
