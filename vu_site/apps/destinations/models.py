from django.db import models
from apps.main.utils import upload_file_path
from django.core.validators import RegexValidator
# Create your models here.


class Destination(models.Model):
    slug = models.SlugField(max_length=255, null=False, unique=True)
    name = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    image = models.ImageField(upload_to=upload_file_path, blank=False, null=False)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class Service(Destination):
    location = models.CharField(max_length=255, null=False, blank=False)
    phone_regex = RegexValidator(regex=r'^360-650-\d{4}$',
                                 message="Phone number must be in format 360-650-xxxx.")
    phone_number = models.CharField(max_length=12,validators=[phone_regex], blank=True)