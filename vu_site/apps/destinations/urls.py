from django.conf.urls import include, url
from .views import ServiceListView, ServiceDetailView

urlpatterns = [
    url(r'^$', 'apps.destinations.views.discover', name='discover'),
    url(r'^$', 'apps.destinations.views.plan', name='plan'),
    url(r'^/services/$', ServiceListView.as_view(), name='service-list'),
    url(r'^/services/(?P<slug>[-\w]+)/$', ServiceDetailView.as_view(), name='service-detail'),
]