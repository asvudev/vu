# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('destinations', '0002_auto_20161201_2102'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='phone_number',
            field=models.CharField(blank=True, max_length=12, validators=[django.core.validators.RegexValidator(regex=b'^360-650-\\d{4}$', message=b'Phone number must be in format 360-650-xxxx.')]),
        ),
    ]
