# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|

  config.vm.box = "ubuntu/trusty64"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  #config.vm.network "forwarded_port", guest: 8000, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: (ENV['VU_SITE_IP'] || "192.168.33.50")


  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
     sudo apt-get update
     sudo apt-get install -y virtualenv

     sudo apt-get update
     sudo apt-get install -y python-setuptools
     sudo apt-get install -y python-virtualenv

     sudo apt-get install -y python-dev

     #required for python-ldap
     sudo apt-get install -y libldap2-dev
     sudo apt-get install -y libsasl2-dev

     #needed to install the ldap-auth from github
     sudo apt-get install -y git

     #needed to install the django-cas from bitbucket (using hg)
     sudo apt-get install -y mercurial

    #mariaDB 5.5
     #we need to set the root password for the mariadb install. the password is 'localhost'
     export DEBIAN_FRONTEND=noninteractive
     sudo debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password password localhost'
     sudo debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password_again password localhost'
     sudo apt-get install -y mariadb-server

     #there are required for pip to install / compile the packages for mariadb
     sudo apt-get install -y libssl-dev
     sudo apt-get install -y python-dev
     sudo apt-get install -y libmariadbclient-dev

    #Bind mariadb to all network interfaces
    # (replace 127.0.0.1 with 0.0.0.0) in the my.conf file
    sudo sed -i 's/127\.0\.0\.1/0\.0\.0\.0/' /etc/mysql/my.cnf
    sudo service mysql restart

    #Allow root to connect to the DB from the host machine
    mysql -u root -plocalhost -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'localhost' WITH GRANT OPTION;FLUSH PRIVILEGES;"

    mysql -u root -plocalhost -e "CREATE DATABASE vu_site_dev;"


    echo DJANGO_SETTINGS_MODULE=config.settings.dev >> /etc/environment
    echo DB_URI=mysql://root:localhost@localhost:3306/vu_site_dev >> /etc/environment

    export DJANGO_SETTINGS_MODULE=config.settings.dev
    export DB_URI=mysql://root:localhost@localhost:3306/vu_site_dev

    echo MEDIA_ROOT=/home/vagrant/media/ >> /etc/environment
    echo MEDIA_URL=/media/ >> /etc/environment

    export MEDIA_ROOT=/home/vagrant/media/
    export MEDIA_URL=/media/

    mkdir /home/vagrant/media
    mkdir /home/vagrant/media/up

    chown -R vagrant /home/vagrant
    chgrp -R vagrant /home/vagrant
    # create the virtual environment
     sudo -u vagrant virtualenv --no-site-packages /home/vagrant/virtualenv
     chown -R vagrant:vagrant /home/vagrant/virtualenv

    # install python packages:
    /home/vagrant/virtualenv/bin/pip install -r /vagrant/requirements/dev.txt

    #Do Initial migration
    cd /vagrant/vu_site
    /home/vagrant/virtualenv/bin/python manage.py migrate



   SHELL
end
