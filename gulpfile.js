var gulp = require('gulp');
var sass = require('gulp-sass');
var bower = require('gulp-bower');


gulp.task('default', ['build', 'watch']);

gulp.task('build', ['install', 'sass', 'copy-foundation-fonts']);

gulp.task('watch', ['build'], function(){
  gulp.watch('vu_site/static/src/sass/**/*.scss', ['sass']);
  gulp.watch('vu_site/static/src/sass/**/*.sass', ['sass']);
});

gulp.task('sass', ['install'], function () {
    return gulp.src('vu_site/static/src/sass/*.sass')
        .pipe(sass())
        .pipe(gulp.dest('./compiled_static/css/'));
});

gulp.task('install', ['bower']);

gulp.task('bower', function() {
  return bower();
});

gulp.task('copy-foundation-fonts', ['bower'], function() {
    return gulp
    .src('bower_components/foundation-icon-fonts/foundation-icons.+(eot|svg|ttf|woff)')
    .pipe(gulp.dest('./compiled_static/css/fonts'))
});
